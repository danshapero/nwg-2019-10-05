{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook\n",
    "import firedrake\n",
    "from firedrake import inner, grad, dx, ds, exp, derivative, Constant, interpolate"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Create a domain and a discretization.\n",
    "In this case, the domain is a triangulation of the unit square and the finite element basis functions are piecewise linears on triangles."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nx, ny = 64, 64\n",
    "mesh = firedrake.UnitSquareMesh(nx, ny)\n",
    "Q = firedrake.FunctionSpace(mesh, family='CG', degree=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Create a function `u_ext` representing the temperature of the exterior of the domain.\n",
    "The boundary conditions for this problem will be\n",
    "\n",
    "$$-k\\nabla u\\cdot n = h(u - u_\\text{ext}),$$\n",
    "\n",
    "where $n$ is the unit outward normal vector and $h$ is the thermal exchange coefficient."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x, y = firedrake.SpatialCoordinate(mesh)\n",
    "\n",
    "δu = Constant(0.0)\n",
    "u_ext = -1 + δu * exp(-4 * ((x - 0.5)**2 + (y - 1)**2))\n",
    "\n",
    "h = Constant(10.0)\n",
    "ϵ = Constant(1/32)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Create the initial values of the temperature and phase field, as well as some variable to hold the old values for timestepping."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u = interpolate(Constant(-1), Q)\n",
    "u0 = u.copy(deepcopy=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ϕ = interpolate(Constant(-1), Q)\n",
    "ϕ0 = ϕ.copy(deepcopy=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "timestep = 0.005\n",
    "δt = Constant(timestep)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We want the thermal conductivity $k$ to go from 1.0 in the solid phase to 0.25 in the liquid phase.\n",
    "(The thermal conductivity of ice is about four times that of water.)\n",
    "To create this effect, we'll make the thermal conductivity a function of the phase field that switches using the hyperbolic tangent function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def tanh(z):\n",
    "    return (exp(z) - exp(-z)) / (exp(z) + exp(-z))\n",
    "\n",
    "k = 0.25 + 0.75 * (tanh(-5 * ϕ) + 1) / 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now the good part!\n",
    "First, the heat flow part can be represented as the minimization, at every timestep, of the following quadratic functional:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "J = 0.5 * (u - u0)**2 * dx + 0.5 * δt * (k * inner(grad(u), grad(u)) * dx + h * (u - u_ext)**2 * ds)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The phase field evolution is a bit harder.\n",
    "The full problem is to solve\n",
    "\n",
    "$$\\tau\\frac{\\partial\\phi}{\\partial t} = \\varepsilon^2\\nabla^2\\phi + \\phi(1 - \\phi^2) + u.$$\n",
    "\n",
    "(We're taking $\\tau = 1$ here but this should be made adjustable.)\n",
    "Rather than solve this PDE directly we'll use an operator splitting approach.\n",
    "First, we need to solve the nonlinear ODE\n",
    "\n",
    "$$\\frac{\\partial\\phi}{\\partial t} = \\phi(1 - \\phi^2) + u.$$\n",
    "\n",
    "This is a stiff problem so we'll use the implicit Euler method:\n",
    "\n",
    "$$\\frac{\\phi_{n + 1} - \\phi_n}{\\delta t} = \\phi_{n + 1}(1 - \\phi_{n + 1}^2) + u$$\n",
    "\n",
    "which is a polynomial equation for $\\phi_{n + 1}$.\n",
    "The function `separate` below uses a Newton iteration to solve this polynomial equation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def separate(δt, φ0, u, tolerance=1e-10, max_iterations=30):\n",
    "    φ = φ0.copy(deepcopy=True)\n",
    "    Q = φ.function_space()\n",
    "    F = φ - φ0 - δt * (φ * (1 - φ**2) + u)\n",
    "    dF_dφ = 1 - δt + 3 * δt * φ**2\n",
    "\n",
    "    error = firedrake.norm(F)\n",
    "    iteration = 0\n",
    "    while (error > tolerance) and (iteration < max_iterations):\n",
    "        φ.assign(firedrake.interpolate(φ - F / dF_dφ, Q))\n",
    "        error = firedrake.norm(F)\n",
    "        iteration += 1\n",
    "\n",
    "    return φ"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next is the phase diffusion step, which solves\n",
    "\n",
    "$$\\frac{\\partial\\phi}{\\partial t} = \\varepsilon^2\\nabla^2\\phi.$$\n",
    "\n",
    "If we alternate the separation and diffusion steps we get a first-order consistent method for the whole problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def diffuse(δt, φ0, ε):\n",
    "    Q = φ0.function_space()\n",
    "    φ, ψ = firedrake.TrialFunction(Q), firedrake.TestFunction(Q)\n",
    "    a = (φ * ψ + δt * ε**2 * inner(grad(φ), grad(ψ))) * dx\n",
    "    L = φ0 * ψ * dx\n",
    "    φ = φ0.copy(deepcopy=True)\n",
    "    firedrake.solve(a == L, φ)\n",
    "    return φ"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The loop to actually solve everything.\n",
    "The exterior temperature is increased along the boundary $y = 1$ until the surrounding medium is at a temperature of +2${}^\\circ$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "final_time = 10.0\n",
    "num_steps = int(final_time / timestep)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "us = [u0.copy(deepcopy=True)]\n",
    "ϕs = [ϕ0.copy(deepcopy=True)]\n",
    "\n",
    "δu_δt = Constant(1.)\n",
    "\n",
    "for step in range(num_steps):\n",
    "    t = (step + 1) * timestep\n",
    "    if t < 3.0:\n",
    "        δu.assign(δu + δu_δt * δt)\n",
    "        \n",
    "    firedrake.solve(derivative(J, u) == 0, u)\n",
    "    u0.assign(u)\n",
    "    \n",
    "    ϕ.assign(separate(δt, ϕ, u, tolerance=1e-4))\n",
    "    ϕ.assign(diffuse(δt, ϕ, ϵ))\n",
    "    ϕ0.assign(ϕ)\n",
    "    \n",
    "    if step % 50 == 0:\n",
    "        print('{: <+8.4g}  {: <+8.4g}'\n",
    "              .format(u.dat.data_ro.min(), u.dat.data_ro.max()))\n",
    "\n",
    "    us.append(u.copy(deepcopy=True))\n",
    "    ϕs.append(ϕ.copy(deepcopy=True))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot everything and make a movie."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "firedrake.plot(u, plot3d=True, cmap='magma')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "firedrake.plot(ϕ, plot3d=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from mpl_toolkits.mplot3d import Axes3D\n",
    "import matplotlib.animation as animation\n",
    "import matplotlib.tri\n",
    "import matplotlib"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = mesh.coordinates.dat.data_ro\n",
    "x, y = X[:, 0], X[:, 1]\n",
    "triangles = mesh.coordinates.cell_node_map().values\n",
    "triangulation = matplotlib.tri.Triangulation(x, y, triangles)\n",
    "\n",
    "fig = plt.figure(figsize=(8.4, 4.2))\n",
    "ax_u = fig.add_subplot(1, 2, 1, projection='3d')\n",
    "ax_u.set_zlim(-1., +1.5)\n",
    "ax_ϕ = fig.add_subplot(1, 2, 2, projection='3d', sharex=ax_u, sharey=ax_u)\n",
    "ax_ϕ.set_zlim(-1.25, +1.25)\n",
    "\n",
    "plots = [ax_u.plot_trisurf(triangulation, us[0].dat.data_ro, vmin=-1., vmax=+2.0,\n",
    "                           edgecolor='none', cmap='magma', antialiased=False),\n",
    "         ax_ϕ.plot_trisurf(triangulation, ϕs[0].dat.data_ro, vmin=-1.25, vmax=+1.25,\n",
    "                           edgecolor='none', cmap='RdBu_r', antialiased=False)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "frames_per_sec = 24\n",
    "\n",
    "def update_plot(frame_number, temperatures, phases, plots):\n",
    "    plots[0].remove()\n",
    "    plots[0] = ax_u.plot_trisurf(\n",
    "        triangulation, temperatures[frame_number].dat.data_ro,\n",
    "        vmin=-1., vmax=+2.0, edgecolor='none', cmap='magma', antialiased=False\n",
    "    )\n",
    "    \n",
    "    plots[1].remove()\n",
    "    plots[1] = ax_ϕ.plot_trisurf(\n",
    "        triangulation, phases[frame_number].dat.data_ro,\n",
    "        vmin=-1.25, vmax=+1.25, edgecolor='none', cmap='RdBu_r', antialiased=False\n",
    "    )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "anim = animation.FuncAnimation(fig, update_plot, num_steps, fargs=(us, ϕs, plots),\n",
    "                               interval=1000/frames_per_sec, repeat=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "anim.save('figures/phase-field.mp4', writer='ffmpeg', fps=frames_per_sec)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "firedrake",
   "language": "python",
   "name": "firedrake"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
