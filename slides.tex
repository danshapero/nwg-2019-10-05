%!TEX program = xelatex
\documentclass{beamer}

\usepackage{hyperref}

\usetheme{Execushares}

\title{Phase field methods}
\subtitle{A simple solution to  dynamic geometries}
\author{Daniel Shapero}
\date{2019 October 5}

\setcounter{showSlideNumbers}{1}

\begin{document}
    \setcounter{showProgressBar}{0}
    \setcounter{showSlideNumbers}{0}

    \frame{\titlepage}

    \begin{frame}
        \frametitle{Contents}
        \begin{enumerate}
            \item Introduction \\ \textcolor{ExecusharesGrey}{\footnotesize\hspace{1em} The Stefan problem}
            \item State of the art \\ \textcolor{ExecusharesGrey}{\footnotesize\hspace{1em} Front-tracking and level-set methods}
            \item Phase-field methods \\ \textcolor{ExecusharesGrey}{\footnotesize\hspace{1em} with demonstration}
            \item Next steps \\ \textcolor{ExecusharesGrey}{\footnotesize\hspace{1em} Towards calving models}
        \end{enumerate}
    \end{frame}

    \setcounter{framenumber}{0}
    \setcounter{showProgressBar}{1}
    \setcounter{showSlideNumbers}{1}
    \section{Introduction}
        \begin{frame}
            \frametitle{Introduction}
            \begin{itemize}
                \item I'm going to talk about a trick in computational physics called the phase field method.
                \item It's a solution for problems with \emph{dynamic geometry}.
                \item To illustrate, a familiar problem: heat conduction.
            \end{itemize}
        \end{frame}

        \begin{frame}
            \frametitle{Cast of characters}
            \begin{itemize}
                \item $T$: temperature
                \item $\rho$: density
                \item $c$: specific heat capacity [energy / (mass $\cdot$ temperature)]
                \item $k$: thermal conductivity [power / (length $\cdot$ temperature)]
                \item $q$: heat sources [power]
                \item $T_m$: melting point
            \end{itemize}
        \end{frame}

        \begin{frame}
            \frametitle{An example}
            \begin{itemize}
                \item Temperature of a material in steady state satisfies the heat equation:
                    \begin{equation}
                        0 = \nabla\cdot k\nabla T + q
                    \end{equation}
                \item What if we're looking at two materials in contact?
                \item They don't have the same $k$! Ex, for H2O:
                    \begin{equation}
                        k(T) = \begin{cases}0.5918 & T > 0^\circ C \\ 2.2 & T < 0^\circ C\end{cases} \hspace{5pt}\frac{W}{m\hspace{5pt}^\circ K}
                    \end{equation}
                \item No problem, we need the heat fluxes to cancel:
                    \begin{equation}
                        0 = (k\nabla T)_\text{solid} - (k\nabla T)_\text{liquid}.
                    \end{equation}
            \end{itemize}
        \end{frame}

        \begin{frame}
            \frametitle{The Stefan problem}
            \begin{itemize}
                \item What if we're no longer in steady state?
                    \begin{equation}
                        \frac{\partial}{\partial t}\rho cT = \nabla\cdot k\nabla T + q
                    \end{equation}
                \item What happens at the phase boundary?
                    Fancy math $\Rightarrow$
                    \begin{equation}
                        \Delta E\cdot v = (k\nabla T)_\text{solid} - (k\nabla T)_\text{liquid}.
                    \end{equation}
                    where
                    \begin{itemize}
                        \item $\Delta E = \rho L = $ jump in internal energy at $T_m$
                        \item \textbf{$v = $ velocity of the phase boundary.}
                    \end{itemize}
                \item The geometry of the problem is changing in time!
            \end{itemize}
        \end{frame}

        \begin{frame}
            \frametitle{Dynamic geometries}
            \begin{itemize}
                \item Phase change is an example of a problem where the geometry is a variable that has its own dynamics.
                \item \textbf{Coping with a changing geometry is one of the hardest problems in computational physics.}
            \end{itemize}
        \end{frame}

        \begin{frame}
            \frametitle{Examples}
            \begin{itemize}
                \item Here we'll focus on phase change in materials.
                \item But my real goal is modeling glacier termini.
                \item Some unrelated but cool engineering problems:
                    \begin{enumerate}
                        \item fluid-structure interaction (turbines, windmills)
                        \item shape and topology optimization
                    \end{enumerate}
            \end{itemize}
        \end{frame}

        \begin{frame}
            \frametitle{Glacier termini}
            Columbia Glacier, AK

            \begin{center}
                \begin{onlyenv}<1>
                    \includegraphics[width=0.8\linewidth]{figures/columbia_tm5_2005245_lrg.jpg}

                    2005
                \end{onlyenv}
                \begin{onlyenv}<2>
                    \includegraphics[width=0.8\linewidth]{figures/columbia_oli_2013203_lrg.jpg}

                    2013
                \end{onlyenv}
            \end{center}

            \tiny{from NASA Earth Observatory}
        \end{frame}

    \section{State of the art}
        \begin{frame}
            \frametitle{Approaches}
            There are a few common ways to deal with dynamic geometries:
            \begin{itemize}
                \item front tracking, cut-cell (explicit)
                \item level-set methods (implicit)
            \end{itemize}
        \end{frame}

        \begin{frame}
            \frametitle{Front tracking}
            \begin{itemize}
                \item Explicitly tracks where the phase boundary is.
                \item Problem: computational geometry is hard.
            \end{itemize}

            \begin{center}
                \includegraphics[width=0.65\linewidth]{figures/cut-cell.pdf}

                \tiny{from Ingram et al. 2003, Developments in Cartesian cut cell methods}
            \end{center}

        \end{frame}

        \begin{frame}
            \frametitle{Implicit methods}
            \begin{itemize}
                \item Rather than explicitly describe the interface, introduce a new field $\phi$.
                    \textbf{The 0-level set of $\phi$ is the phase boundary.}
                \item This field evolves via the level set equation:
                    \begin{equation}
                        \frac{\partial\phi}{\partial t} = v|\nabla\phi|.
                    \end{equation}
                \item Advantage: easier than computational geometry -- lots of people do it, e.g. ISSM
                \item Problem: how to initialize $\phi$?
                    When to re-initialize?
            \end{itemize}
        \end{frame}

        \begin{frame}
            \frametitle{Illustration}
            \begin{center}
                \includegraphics[width=0.8\linewidth]{figures/level-set.png}
            \end{center}

            \tiny{From Wikipedia/Level set method}
        \end{frame}

    \section{Phase field methods}
        \begin{frame}
            \frametitle{Introduction}
            \begin{itemize}
                \item The phase field method is an alternative implicit method.
                \item It's inspired by statistical mechanics, in particular the concept of minimizing the \emph{free energy}.
            \end{itemize}
        \end{frame}

        \begin{frame}
            \frametitle{Free energy}
            \begin{itemize}
                \item Three components of the free energy:
                    \begin{itemize}
                        \item Two stable equilibria: $\phi = -1$ $\Rightarrow$ solid, $\phi = +1$ $\Rightarrow$ liquid.
                        \item Temperature should favor one equilibrium over the other.
                        \item It should be a smooth function.
                    \end{itemize}
                \item We can sum all this up in one functional:
                    \begin{equation}
                        F(\phi) = \int_\Omega\left(\frac{\epsilon^2}{2}|\nabla\phi|^2 + \frac{1}{4}(1 - \phi^2)^2 - \alpha(T - T_m)\phi\right)dx
                    \end{equation}
            \end{itemize}
        \end{frame}

        \begin{frame}
            \frametitle{Free energy}
            \begin{center}
                \includegraphics[width=0.7\linewidth]{figures/potential-well.pdf}
            \end{center}

                There are two stable equilibria for $\phi$, one above and one below 0, but the temperature makes one equilibrium have a lower free energy than the other.
        \end{frame}

        \begin{frame}
            \frametitle{Dynamics}
            \begin{itemize}
                \item The phase field evolves to minimize the free energy:
                    \begin{equation}
                        \tau\frac{\partial\phi}{\partial t} = -\frac{dF}{d\phi}.
                    \end{equation}
                    where $\tau$ is some time scale.
                \item There's one extra term in the heat equation:
                    \begin{equation}
                        \frac{\partial}{\partial t}(\rho cT + \textcolor{red}{\rho L\phi} / 2) = \nabla\cdot k\nabla T + q
                    \end{equation}
                \item As $\tau$, $\epsilon \to 0$, we get back the Stefan problem!
            \end{itemize}
        \end{frame}

        \begin{frame}
            \frametitle{Example}
            \begin{itemize}
                \item Let's see a simple example in 2D!
                \item Setup: a solid box, heated on the top edge until it melts.
                \item \textcolor{ExecusharesRed}{\href{https://youtu.be/u451VBlnuig}{results on YouTube}}
                \item \textcolor{ExecusharesRed}{\href{https://gitlab.com/danshapero/nwg-2019-10-05}{code on GitLab}}
            \end{itemize}
        \end{frame}

        \begin{frame}
            \frametitle{No free lunch}
            \begin{itemize}
                \item Phase field methods are a relatively simple way to solve moving boundary problems.
                \item The PDE is easier than the level set equation.
                \item The price is a diffuse interface of width $\sim\epsilon$.
            \end{itemize}
        \end{frame}


    \section{Next steps}
        \begin{frame}
            \frametitle{Calving}
            \begin{itemize}
                \item Iceberg calving $\Rightarrow$ a dynamic boundary!
                \item ``Cold'' phase = glacier, ``warm'' phase = ice melange
                \item What is the analogue of $T$, $T_m$?
            \end{itemize}
        \end{frame}

        \begin{frame}
            \frametitle{Calving by height above flotation}
            \begin{itemize}
                \item Some calving models make icebergs break off when the ice thins below a critical height $h_c$ above flotation.
                \item This works well for high-frequency, low-amplitude calving, like in Greenland.
                \item The forcing term in the phase field model then becomes:
                    \begin{equation}
                        \tau\frac{d\phi}{dt} = \ldots + \alpha(h_c - h)
                    \end{equation}
                \item What about for Antarctic-type calving?
            \end{itemize}
        \end{frame}

        \begin{frame}
            \frametitle{Calving by damage mechanics}
            \begin{itemize}
                \item \emph{Continuum damage mechanics} describes effect of crevasses on glacier flow.
                \item A new field $D$ represents net mechanical decoupling; $D = 0$ $\Rightarrow$ no damage, $D = 1$ $\Rightarrow$ totally cracked.
                \item Damage grows when stresses are high and shrinks when the strain rate is negative:
                    \begin{equation}
                        \frac{dD}{dt} = \gamma_+(\tau - \tau_c)_+(1 - D) - \gamma_-(\dot\varepsilon_c - \dot\varepsilon)_-D
                    \end{equation}
                \item A calving law: break when $D > D_c \approx 0.8$.
            \end{itemize}
        \end{frame}

        \begin{frame}
            \frametitle{Illustration}
            Damage propagation through a synthetic ice shelf; ice flows in through two streams along the semi-circular boundary

            \begin{center}
                \includegraphics[width=0.48\linewidth]{figures/ice-shelf-thickness.png}
                \includegraphics[width=0.48\linewidth]{figures/ice-shelf-damage.png}
            \end{center}

            courtesy of Andrew Hoffmann
        \end{frame}

        \begin{frame}
            \frametitle{Summary}
            \begin{itemize}
                \item Phase fields could be a nice way to implement calving.
                \item But all they do is handle the dynamic geometry.
                \item The hard physics of deciding what variables dictate calving and how still has to be done.
            \end{itemize}
        \end{frame}

        \begin{frame}
            \frametitle{Shameless plug}
            \begin{center}
                \huge{icepack.github.io}

                ${}$

                \huge{shapero@uw.edu}
            \end{center}
        \end{frame}

\end{document}
