import numpy as np
import matplotlib.pyplot as plt

z = np.linspace(-1.5, +1.5, 161)
f1 = (1 - z**2)**2 / 4
u = 0.2
f2 = (1 - z**2)**2 / 4 - u * z

fig, axes = plt.subplots()
axes.plot(z, f1, label=r'$T_m$')
axes.plot(z, f2, label=r'$T_m + \delta T$')
axes.set_xlabel(r'Order parameter $\phi$')
axes.set_ylabel(r'Free energy $F$')
axes.legend()
fig.savefig('potential-well.pdf', bbox_inches='tight', transparent=True)
